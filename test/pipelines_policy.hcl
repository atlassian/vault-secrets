# ACL policies

# repo level secret
path "secret/data/pipelines/{{identity.entity.aliases.auth_jwt_8200b41a.metadata.workspaceUuid}}/repositories/{{identity.entity.aliases.auth_jwt_8200b41a.metadata.repositoryUuid}}/variables/*" {
  capabilities = [ "create", "read", "update"]
}

# workspace level secret
path "secret/data/pipelines/{{identity.entity.aliases.auth_jwt_8200b41a.metadata.workspaceUuid}}/variables/*" {
  capabilities = [ "create", "read", "update", "list" ]
}

# branch level secret
path "secret/data/pipelines/{{identity.entity.aliases.auth_jwt_8200b41a.metadata.workspaceUuid}}/repositories/{{identity.entity.aliases.auth_jwt_8200b41a.metadata.repositoryUuid}}/branches/{{identity.entity.aliases.auth_jwt_8200b41a.metadata.branchName}}/variables/*" {
    capabilities= ["read"]
}

# custom test secret
path "secret/data/*" {
    capabilities= ["read"]
}
