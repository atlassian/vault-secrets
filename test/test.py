import os

from bitbucket_pipes_toolkit.test import PipeTestCase


class VaultPipeTestCase(PipeTestCase):
    VAULT_TOKEN = os.getenv('VAULT_DEV_ROOT_TOKEN_ID')
    VAULT_ADDR = 'http://host.docker.internal:8200'
    VAULT_OIDC_ROLE = 'pipelines'
    VAULT_AUTH_PATH = os.getenv('VAULT_AUTH_PATH')
    OIDC_TOKEN = os.getenv('BITBUCKET_STEP_OIDC_TOKEN')

    TEST_KEY = 'passcode'
    TEST_VALUE = 'test-secret-passcode'
    TEST_PATH = 'secret/creds'
    TEST_FAILED_PATH = 'foo/creds'

    def test_get_secrets_success(self):
        result = self.run_container(
            environment={
                'VAULT_ADDRESS': self.VAULT_ADDR,
                'VAULT_TOKEN': self.VAULT_TOKEN,
                'VAULT_AUTH_PATH': self.VAULT_AUTH_PATH,
                'SECRET_PATH': self.TEST_PATH
            },
            extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')}
        )

        self.assertIn('Your secrets are saved!', result)

    def test_get_secrets_failed_path(self):
        result = self.run_container(
            environment={
                'VAULT_ADDRESS': self.VAULT_ADDR,
                'VAULT_TOKEN': self.VAULT_TOKEN,
                'VAULT_AUTH_PATH': self.VAULT_AUTH_PATH,
                'SECRET_PATH': self.TEST_FAILED_PATH
            },
            extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')}
        )

        self.assertIn(f'No secrets under secret path: {self.TEST_FAILED_PATH}.', result)

    def test_get_secrets_oidc_auth_success(self):
        result = self.run_container(
            environment={
                'VAULT_ADDRESS': self.VAULT_ADDR,
                'VAULT_AUTH_PATH': self.VAULT_AUTH_PATH,
                'VAULT_OIDC_ROLE': self.VAULT_OIDC_ROLE,
                'BITBUCKET_STEP_OIDC_TOKEN': self.OIDC_TOKEN,
                'SECRET_PATH': self.TEST_PATH
            },
            extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')}
        )

        self.assertIn('Your secrets are saved!', result)

    def test_get_secrets_oidc_auth_fail(self):
        result = self.run_container(
            environment={
                'VAULT_ADDRESS': self.VAULT_ADDR,
                'VAULT_AUTH_PATH': self.VAULT_AUTH_PATH,
                'VAULT_OIDC_ROLE': self.VAULT_OIDC_ROLE,
                'BITBUCKET_STEP_OIDC_TOKEN': 'foo',
                'SECRET_PATH': self.TEST_PATH
            },
            extra_hosts={"host.docker.internal": os.getenv('BITBUCKET_DOCKER_HOST_INTERNAL')}
        )

        self.assertIn('Auth token error', result)
