# Bitbucket Pipelines Pipe: Vault secrets

Fetch your secrets from the [HashiCorp vault][HashiCorp vault]. The pipe supports Bitbucket OIDC and TOKEN authentication.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/vault-secrets:0.6.1
  variables:
    VAULT_ADDRESS: "<string>"
    SECRET_PATH: "<string>"
    VAULT_TOKEN: "<string>" # Optional. Required if no oidc and auth is by vault token.
    VAULT_OIDC_ROLE: "<string>" # Optional, should be provided with BITBUCKET_STEP_OIDC_TOKEN. Required if auth is by oidc.
    BITBUCKET_STEP_OIDC_TOKEN: "<string>" # Optional if already defined in the context. Required if auth is by oidc.
    # VAULT_AUTH_PATH: "<string>" # Optional. Allows to set non-default vault auth path.
    # VAULT_NAMESPACE: "<string>" # Optional
    # DEBUG: "<boolean>" # Optional
```

## Variables

| Variable                          | Usage                                                                                                                                          |
|-----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------|
| VAULT_ADDRESS (*)                 | Address of your vault storage.                                                                                                                 |
| SECRET_PATH (*)                   | Path to your secrets in vault kv secret engine.                                                                                                |
| VAULT_TOKEN (1)                   | Method for authentication to read secrets from Vault. Required if no oidc and auth is by vault token.                                          |
| VAULT_OIDC_ROLE (1)               | ACL role name in vault with required permissions. Required if authorization type is oidc. Should be provided with `BITBUCKET_STEP_OIDC_TOKEN`. |
| BITBUCKET_STEP_OIDC_TOKEN (1) (2) | Bitbucket generated oidc token. Should be provided with `VAULT_OIDC_ROLE`.                                                                     |
| VAULT_AUTH_PATH                   | Customizable [vault auth path][vault auth path]. Provide if auth methods are mounted to custom path.                                           |
| VAULT_NAMESPACE                   | Represents Vault Enterprise namespace. Default: `''`.                                                                                          |
| DEBUG                             | Turn on extra debug information. Default: `false`.                                                                                             |

_(*) = required variable._

_(1) = required variable. Required one of the multiple options._

_(2) = If this variable is configured as a repository, account or environment variable, it does not need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._

## Important note - hiding secrets from logs
This pipe does NOT mask the secrets from logs yet as other pipeline secure variables. So, every CI/CD command that receives secrets input should be reviewed carefully to avoid printing anything sensitive into logs.

## Prerequisites
Note: secret engine vault types supported in this pipe: `kv`.
### JWT (OIDC) authentication
To enable OIDC through jwt on vault:
```
vault auth enable jwt
vault write auth/jwt/config jwks_url="https://api.bitbucket.org/2.0/workspaces/<your_workspace>/pipelines-config/identity/oidc/keys.json" bound_issuer="https://api.bitbucket.org/2.0/workspaces/<your_workspace>/pipelines-config/identity/oidc"
```

### Policy creation
To create your own policy on vault:
```
vault policy write <your_policy_name> ./pipelines_policy.hcl
```

Example of policy: [Policy example][policy template].

### Role creation
To create role:
```
vault write auth/jwt/role/pipelines @role.json
```
Role example with policy named `pipelines`: [Role example][role template].

For more information how to create role and bound audience in vault check [How to create role in vault][vault role].

Note: be sure to provide access in policies to your data. In example, if you stored your secrets under `SECRET_PATH`: `secret/foo`
then you need to have in your used role policy:
```
path "secret/data/foo" {
    capabilities= ["read"]
}
```

## Examples
Basic example:

```yaml
script:
  - pipe: atlassian/vault-secrets:0.6.1
    variables:
      VAULT_ADDRESS: "<your vault address>"
      VAULT_TOKEN: "<your vault token>"
      SECRET_PATH: "secret/foo"
```

Store environs with exporting them via `source <path to your sercrets>`, check the path to generated secrets file in log message.

Basic example with oidc:

```yaml
oidc: true
script:
  - pipe: atlassian/vault-secrets:0.6.1
    variables:
      VAULT_ADDRESS: "<your vault address>"
      VAULT_OIDC_ROLE: "<your created role name with attached policies in vault>"
      SECRET_PATH: "secret/foo"
```

Basic example with oidc and auth mounted to path `auth/my-custom-jwt-path`:

```yaml
oidc: true
script:
  - pipe: atlassian/vault-secrets:0.6.1
    variables:
      VAULT_ADDRESS: "<your vault address>"
      VAULT_OIDC_ROLE: "<your created role name with attached policies in vault>"
      VAULT_AUTH_PATH: "my-custom-jwt-path"
      SECRET_PATH: "secret/foo"
```

Basic example with namespace provided:

```yaml
script:
  - pipe: atlassian/vault-secrets:0.6.1
    variables:
      VAULT_ADDRESS: "<your vault address>"
      VAULT_NAMESPACE: "<your vault namespace>"
      VAULT_TOKEN: "<your vault token>"
      SECRET_PATH: "secret/foo"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2021 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,vault
[HashiCorp vault]: https://www.vaultproject.io
[vault auth path]: https://developer.hashicorp.com/vault/docs/auth#enabling-disabling-auth-methods
[vault role]: https://www.vaultproject.io/api/auth/jwt#create-role
[policy template]: pipelines_policy.hcl.template
[role template]: role.json.template