# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.6.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 0.6.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.5.0

- minor: Added feature to provide custom vault auth path if needed via VAULT_AUTH_PATH.

## 0.4.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.3.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerability with certify.
- minor: Update vault version to 1.14.

## 0.2.0

- minor: Bump vault version to 1.13.2.
- patch: Internal maintenance: bump docker image and packages in requirements.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update release process.

## 0.1.2

- patch: Fix pipe's metadata path.

## 0.1.1

- patch: Update examples in the README

## 0.1.0

- minor: Initial release of Bitbucket Pipelines Vault Secrets pipe.
