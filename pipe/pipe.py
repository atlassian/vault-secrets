import os
import textwrap
from enum import Enum
from enum import auto

import hvac
import yaml
import hvac.exceptions

from bitbucket_pipes_toolkit import Pipe, get_logger


logger = get_logger()

schema = {
    'VAULT_ADDRESS': {'type': 'string', 'required': True},
    'VAULT_NAMESPACE': {'type': 'string', 'default': '', 'required': False},
    'VAULT_OIDC_ROLE': {'type': 'string', 'required': True},
    'BITBUCKET_STEP_OIDC_TOKEN': {'type': 'string', 'required': True},
    'VAULT_AUTH_PATH': {'type': 'string', 'required': False, 'default': None},
    'VAULT_TOKEN': {'type': 'string', 'required': False},  # optional if OIDC is not chosen
    'SECRET_PATH': {'type': 'string', 'required': True},  # path to your secrets
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}

}

# OIDC login inherits JWT https://github.com/hvac/hvac/blob/develop/hvac/api/auth_methods/oidc.py


class AuthMethod(Enum):
    OIDC = auto()
    TOKEN = auto()


class VaultSecretsPipe(Pipe):
    OIDC_AUTH = 'OIDC_AUTH'
    TOKEN_AUTH = 'TOKEN_AUTH'
    AWS_AUTH = 'AWS_AUTH'
    # ... and some others supported methods for the future:
    # https://github.com/hvac/hvac/tree/develop/hvac/api/auth_methods

    SECRETS_DIRECTORY = os.path.join(os.getcwd(), '.secrets')
    ACTIVATE_SECRETS_FILE = os.path.join(SECRETS_DIRECTORY, 'bin', 'activate.sh')
    SECRETS_FILE = os.path.join(SECRETS_DIRECTORY, '.vault-secrets')

    def __init__(self, *args, **kwargs):
        auth_method = self.get_auth_method()

        super().__init__(*args, **kwargs)

        client = hvac.Client(
            url=self.get_variable('VAULT_ADDRESS'),
            namespace=self.get_variable('VAULT_NAMESPACE')
        )

        self.auth_path = self.get_variable('VAULT_AUTH_PATH')

        client.token = self.auth(client, auth_method)

        self.client = client

    @staticmethod
    def get_auth_method():
        """

        :return: AuthMethod member
        """
        oidc_role = os.getenv('VAULT_OIDC_ROLE')
        id_token = os.getenv('BITBUCKET_STEP_OIDC_TOKEN')
        if oidc_role:
            if id_token:
                return AuthMethod.OIDC

            logger.warning('Parameter "oidc: true" in the step configuration is required for OIDC authentication')

        schema['BITBUCKET_STEP_OIDC_TOKEN']['required'] = False
        schema['VAULT_OIDC_ROLE']['required'] = False
        schema['VAULT_TOKEN']['required'] = True
        # vault write auth login. Optional auth. Not the part of POC, but part of the official pipe
        # use https://github.com/hvac/hvac/blob/develop/hvac/api/auth_methods/token.py .
        # Supported auth: https://github.com/hvac/hvac/tree/develop/hvac/api/auth_methods
        logger.info('Using vault token authentication. VAULT_TOKEN is required')

        return AuthMethod.TOKEN

    def auth(self, client, auth_method):
        """

        :param client: hvac.Client
        :param auth_method: member of AuthMethod enum
        :return: str
        """
        if auth_method == AuthMethod.OIDC:
            try:
                token = client.auth.jwt.jwt_login(
                    role=self.get_variable('VAULT_OIDC_ROLE'),
                    jwt=self.get_variable('BITBUCKET_STEP_OIDC_TOKEN'),
                    path=self.auth_path
                )['auth']['client_token']
            except hvac.exceptions.InvalidRequest as e:
                self.fail(f'Auth token error: {str(e)}')

            return token
        elif auth_method == AuthMethod.TOKEN:
            return self.get_variable('VAULT_TOKEN')

    def save_secrets(self, secrets):
        """

        :param secrets: dict containing: {SECRET_KEY: SECRET_VALUE..} . Example: {'USER': 'user', 'PASSWORD': 'pass'}
        :return:
        """
        os.makedirs(os.path.dirname(self.SECRETS_FILE), exist_ok=True)
        os.makedirs(os.path.dirname(self.ACTIVATE_SECRETS_FILE), exist_ok=True)
        with open(self.SECRETS_FILE, 'w') as secret_file:
            secret_values = '\n'.join(f'export {key}={value}' for key, value in secrets.items())
            secret_file.write(f'{secret_values}\n')

        with open(self.ACTIVATE_SECRETS_FILE, 'w') as activate_file:
            string_executable = f"""\
            #!/usr/bin/env sh
            eval '$(cat {self.SECRETS_FILE})'
            rm {self.SECRETS_FILE}
            """
            string_executable = textwrap.dedent(string_executable)
            # construct sh file executable that will substitute the secrets from file and export them
            activate_file.write(string_executable)

    def run(self):
        super().run()

        self.log_info('Starting to retrieve secrets.')

        try:
            mount_point, path = self.get_variable('SECRET_PATH').split('/', 1)
        except ValueError:
            self.fail("Invalid secret path: should contain at least one '/'.")

        try:
            response = self.client.secrets.kv.v2.read_secret(path, mount_point)
        except hvac.exceptions.InvalidPath:
            self.fail(f"No secrets under secret path: {self.get_variable('SECRET_PATH')}.")

        secrets = response['data']['data']

        # put obtained secrets to hidden folder
        self.save_secrets(secrets=secrets)

        self.log_info(f'Your secrets are saved! Activate secrets via: "source {self.ACTIVATE_SECRETS_FILE}"')

        self.success('Pipe has finished successfully.')


if __name__ == '__main__':
    with open('/pipe.yml') as f:
        metadata = yaml.safe_load(f.read())
    pipe = VaultSecretsPipe(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
