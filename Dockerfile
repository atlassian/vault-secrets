FROM python:3.10-slim as build

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
COPY pipe/ requirements.txt LICENSE.txt pipe.yml README.md /
# install requirements
RUN apt-get -y update && apt-get -y install --no-install-recommends curl=7.* \
    software-properties-common=0.* \
    unzip=6.* \
    gnupg=2.* && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    pip install --user --no-cache-dir -r /requirements.txt && \
    curl "https://releases.hashicorp.com/vault/1.14.1/vault_1.14.1_linux_amd64.zip" -o "vault.zip" && \
    echo '6031432dfc3de07f6523d206c44fc018aa969d94c8e9125a77340af359f57ea3 vault.zip' | sha256sum -c - && \
    unzip vault.zip


FROM python:3.10-slim

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

WORKDIR /

# copy vault binary
COPY --from=build /vault /usr/bin

# copy python env
COPY --from=build /root/.local /root/.local
# copy project files
COPY --from=build pipe.py /
COPY --from=build LICENSE.txt pipe.yml README.md /

ENV PATH=/root/.local/bin:$PATH


COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

ENTRYPOINT ["python", "/pipe.py"]